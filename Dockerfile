FROM ruby:2.3.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN gem install bundler --no-ri --no-rdoc

# Cache bundle install
WORKDIR /tmp
ADD ./src/Gemfile Gemfile
ADD ./src/Gemfile.lock Gemfile.lock
RUN bundle install

# Add rails project to project directory
WORKDIR /app
ADD ./src /app
ADD ./prod.run.sh /app
RUN chmod +x prod.run.sh

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*

EXPOSE 3000