#!/bin/bash
rm -f tmp/pids/server.pid

if [ -f /install.txt ]
then
     echo "Install file - Found, nothing to do here"
else
   sleep 10
   echo "Install file - Not found, making initial db:create && db:migrate"
   RAILS_ENV=production bundle exec rake db:create
   RAILS_ENV=production bundle exec rake db:migrate
   RAILS_ENV=production bundle exec rake db:seed
   echo installed > install.txt
fi

bundle exec rake assets:precompile
bundle exec unicorn -E production -c config/unicorn.rb
